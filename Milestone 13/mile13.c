#include<stdio.h>
#include<stdlib.h>
#include<math.h>
struct Node {
	int data;
	struct Node* next;
};
struct Node* insert(struct Node* head,int val);
void print(struct Node* head);
struct Node* reverse(struct Node* current,struct Node* pre);

int main()
{
	struct Node* head;
	head = NULL;
	int ch;
	int n,ele,pos;
	while(ch!=4)
	{
		printf("\n\n1. Insert\n2. Print\n3. Reverse\n Enter choice : ");
		scanf("%d",&ch);
		switch(ch)
		{
			case 1 : 
						printf("\nHow meny numbers you want to enter : ");
						scanf("%d",&n);
						printf("Enter Elements : ");
						for(int i=0;i<n;i++)
						{
							scanf("%d",&ele);
							head = insert(head,ele);
						}
						break;
			case 2 : 
						print(head);
						break;
	
			case 3 :  	head = reverse(head,NULL);
						print(head);
						break;
		}
	}
	
	
}
void print(struct Node* head)
{
	if(head==NULL)
	{printf("\nlist is Empty !!!");}
	else
	{
		struct Node* head_2 = head;
		printf("\nList : ");
		while(head_2!=NULL)
		{
			printf("%d ",head_2->data);
			head_2 = head_2->next;
		}
	}
}
struct Node* insert(struct Node* head,int val)
{
	struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
	temp->data = val;
	temp->next = NULL;
	
	if(head==NULL)
	{
		head=temp;
	}
	else
	{
		struct Node* head_2 = head;
		while(head_2->next!=NULL)
		{
			head_2 = head_2->next;
		}
		head_2->next = temp;
	}
	return head;
}
struct Node* reverse(struct Node* current,struct Node* pre)
{
	struct Node* head;
	if(current->next==NULL)
	{
		current->next=pre;
		head = current;
	}
	else
	{
		head = reverse(current->next,current);
		current->next = pre;
	}
	return head;
}