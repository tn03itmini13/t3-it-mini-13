#include<stdio.h>
double PV(double rate, unsigned int nperiods, double FV);
int main()
{
	double rate,fv,pv;
	unsigned int nperiods;
	
	printf("Enter rate : ");
	scanf("%lf",&rate);
	printf("Enter nperiods : ");
	scanf("%d",&nperiods);
	printf("Enter FV : ");
	scanf("%lf",&fv);
	
	pv = PV(rate,nperiods,fv);
	
	printf("PV : %lf",pv);
}

double PV(double rate, unsigned int nperiods, double fv)
{
	double pv;
	
	pv = fv/((1+rate)*nperiods);

	return pv;
}