#include<stdio.h>
unsigned int fact(unsigned int n);
int main()
{
	unsigned int n,ans;
	printf("Enter number for factorial : ");
	scanf("%d",&n);
	ans = fact(n);
	printf("Factorial is : %d",ans);
}

unsigned int fact(unsigned int n)
{
	unsigned int ans=1;
	while(n>=1)
	{
		ans = ans * n;
		n--;
	}
	return ans;
}